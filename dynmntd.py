#!/usr/bin/env python3

import os, sys, getpass, signal, argparse, yaml
from threading import Event

class Cfg:
  def projectPath(self):
    return os.path.join(self.storagePath, self.projectId)

  def init(self, storagePath, projectId):
    if storagePath:
      self.storagePath = storagePath
    else:
      self.storagePath = os.path.join(os.sep, "home", getpass.getuser(), ".storage")
    self.projectId = projectId
    self.projectIdPath = os.path.join(self.storagePath, self.projectId)

    path = os.path.join(self.storagePath, self.projectId + ".yaml")
    try:
      with open(path, 'r') as file:
        data = yaml.safe_load(file)
        self.networks = data['networks']

        serverData = data['server']
        self.serverUser = serverData['user']
        self.keyfilePath = serverData['keyfile']
        self.serverPath = serverData['path']
        return True
    except FileNotFoundError:
      print("Configuration file '%s' not found. Exit." %path)
    except IOError:
      print("IOError on config load when accessing '%s'", path)
cfg = Cfg()

class ServerMount:
  def __init__(self, networkData):
    self.established = False

    try:
      self.address = networkData['address']
    except KeyError:
      print('Address not specified for network. Fatal configuration error!')
      raise

    try:
      self.port = networkData['port']
    except KeyError:
      self.port = 22

    try:
      self.pingTarget = networkData['ping']
    except KeyError:
      self.pingTarget = self.address

    opts = "-p " + str(self.port)
    opts = opts + " -o reconnect,ServerAliveInterval=15,ServerAliveCountMax=3,BatchMode=yes"
    opts = opts + ",IdentityFile=" + cfg.keyfilePath

    cmd = "sshfs " + opts + " " + cfg.serverUser + "@" + self.address + ":" + cfg.serverPath
    cmd = cmd + " " + cfg.projectIdPath
    self.mountCmd = cmd

  def ping(self):
    def doPing(host, count):
      if os.system("ping -c 1 " + host + " > /dev/null") == 0:
        return True
      else:
        if count == 3:
          # after 3rd fail report problem
          return False
        else:
          return doPing(host, count + 1)

    return doPing(self.pingTarget, 1)

  def mount(self):
    print("mount cmd:", self.mountCmd)
    return os.system(self.mountCmd) == 0

  def establish(self):
    if not self.ping():
      return False

    if os.path.ismount(cfg.projectIdPath):
      if self.established:
        # server connection already established
        return True
      else:
        # mounted through some other connection/process
        return False

    self.established = False

    if self.mount():
      self.established = True
      return True
    else:
      # failed mount
      return False

networks = []
exit = Event()

def checkConnections():
  if os.path.ismount(cfg.projectIdPath):
    return True

  for network in networks:
    if network.establish():
      return True
  return False

## main ##
def eventLoop():
  fails = 0
  while not exit.is_set():
    fails = 0 if checkConnections() else fails + 1
    exit.wait(15 if fails > 3 else 5)

  ## cleanup (give the termination
  ## umount some time before that)
  #sleep(0.5)
  #umount()

def quit(signum, frame):
    exit.set()

if __name__ == "__main__":

  parser = argparse.ArgumentParser(description='Mounts remote directories via sshfs,\
                                    selects route based on ping.')

  parser.add_argument('host',
                      help = 'host identifier, specifies configuration file for this run' )
  parser.add_argument('-s', '--storage', help='storage base path, also place of configuration file')

  args = parser.parse_args()

  if not cfg.init(args.storage, args.host):
    sys.exit(-1)

  for networkData in cfg.networks:
    networks.append(ServerMount(networkData))

  for sig in ('TERM', 'HUP', 'INT'):
      signal.signal(getattr(signal, 'SIG'+sig), quit);
  eventLoop()
