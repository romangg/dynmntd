# dynmntd

Mounts sshfs shares depending on which route is available.

## Example
### Configuration file
Named `nas.yaml` in default location `/home/user/.storage/`:
```
networks:
- address: 192.168.0.1
- address: nas.internal.example.com
  ping: 8.8.8.8
  port: 2222

server:
  user: rmtuser
  keyfile: /home/user/.ssh/id_rsa_nas
  path: /rmt/path/
```

### Systemd user unit
Named `nas.service` in `/home/user/.config/systemd/user/`:
```
[Unit]
AssertPathExists=/home/user/.storage/nas

[Service]
WorkingDirectory=/home/user/.storage
ExecStart=/usr/bin/python3 /home/user/dynmntd/dynmntd.py nas
Restart=always

[Install]
WantedBy=default.target
```

After file creation issue from terminal:
```
# read new service file
$ systemctl --user daemon-reload
# autostart service on login
$ systemctl --user enable nas.service
# also start service now
$ systemctl --user start nas.service
```

## Rerouting
Once a mount has been established for rerouting manual interaction becomes necessary: User must kill the process and if not started as systemd unit unmount with `fusermount -u path-to-mount`, i.e. in above example: `fusermount -u /home/user/.storage/nas`.

When launched as systemd user unit for that just issue in a terminal:
```
$ systemctl --user restart your-service-name.service
```
In above example `systemctl --user restart nas.service`.
